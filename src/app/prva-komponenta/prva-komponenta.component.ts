import { Component, OnInit, NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';


@Component({
  selector: 'app-prva-komponenta',
  templateUrl: './prva-komponenta.component.html',
  styleUrls: ['./prva-komponenta.component.less', '../less/caInfoBox.less', '../less/caBlockList.less', '../less/caInfoBoxHeader.less', '../less/basicDefinitions.less', '../less/cssReset.less']
})
@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrvaKomponentaComponent implements OnInit {

  constructor() {


  }

  ngOnInit() {
  }

}
